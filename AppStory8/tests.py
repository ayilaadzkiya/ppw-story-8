from django.test import TestCase, Client
from .views import List_Books
from django.urls import resolve

class Story8Test(TestCase):

    def test_Story8_url_is_exist(self):
        response = Client().get('/book/')
        self.assertEqual(response.status_code, 200)

    def test_Story8_url_doesnt_exist(self):
        response = Client().get('/buku/')
        self.assertEqual(response.status_code, 404)

    def test_Story8_func(self):
        found = resolve('/book/')
        self.assertEqual(found.func, List_Books)

    def test_Story8_to_do_list_template(self):
        response = Client().get('/book/')
        self.assertTemplateUsed(response, 'Index8.html')